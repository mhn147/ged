import Vue from 'vue'
import Router from 'vue-router'
import DocumentsList from './components/DocumentsList.vue'
import DocumentDetails from './components/DocumentDetails.vue'

Vue.use(Router)

const routes = [
    {path: '/', component: DocumentsList},
    {path: '/document/:id', component: DocumentDetails}
]



export default new Router({  
    routes: routes,
    mode: 'history'
});
