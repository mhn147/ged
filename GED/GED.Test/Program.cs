﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using GED.Data;
using GED.Core;

namespace GED.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Data.Main.Access.Settings.SetConnectionStrings("Server=localhost;Database=GED;Trusted_Connection=True;");

            //var result = GED.Core.Handlers.Document.Documents.Get();

            //List<int> documentsIds = new List<int>() { 1, 14, 15, 20 };

            //List<int> tagsIds = new List<int>() { 3, 4, 5, 6, 7, 8 };

            //List<int> bindersIds = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8 };

            //List<int> languagesIds = new List<int>() { 1, 2, 3, 4, 5 };

            //GED.Data.Main.Access.Tables.Document.InsertDocumentTags(20, tagsIds);

        }


        private static void DatTest()
        {
            //Console.WriteLine("Testing Get By Id");
            //var documentDb = Data.Main.Access.Tables.Document.Get();
            //displayDocument(documentDb);


            //Console.WriteLine("------------------");

            //Console.WriteLine("Testing Get All");
            //var documentsDb = Data.Main.Access.Tables.Document.Get();
            //displayDocuments(documentsDb);

            //Console.WriteLine("------------------");

            //Console.WriteLine("Testing Get some");
            //documentsDb = Data.Main.Access.Tables.Document.Get(new List<int>() { 1, 14, 15 });
            //displayDocuments(documentsDb);



            //var row = new Data.Main.Entities.Tables.Document();
            //row.Id = 135;
            //row.CreationDate = new DateTime(1995, 8, 2, 20, 32, 13, 200);
            //row.AddingTime = new DateTime(1998, 10, 3, 18, 23, 10, 150);
            //row.Title = "NasriMohamed";
            //row.Issuer = "Nasri Mohamed";
            //row.Author = "George R.R. Martin";
            //row.Description = "Ha ha ha ha from code";
            //row.Notes = "ho hi ha from code";
            //Data.Main.Access.Tables.Document.Insert(row);



            //var rowToUpdate = Data.Main.Access.Tables.Document.Get(1);
            //rowToUpdate.Title = "nasrix";
            //Data.Main.Access.Tables.Document.Update(rowToUpdate);


            //Data.Main.Access.Tables.Document.Delete(new List<int>() { 17, 18, 19, 21});


            //IEnumerable<string> binders = Data.Main.Access.Tables.Document.GetDocumentBinders(14);

            //foreach (var binder in binders)
            //{
            //    Console.WriteLine(binder);
            //}

            //IEnumerable<string> languages = Data.Main.Access.Tables.Document.GetDocumentLanguages(1);

            //foreach (var language in languages)
            //{
            //    Console.WriteLine(language);
            //}

            //IEnumerable<string> tags = Data.Main.Access.Tables.Document.GetDocumentTags(15);

            //foreach (var tag in tags)
            //{
            //    Console.WriteLine(tag);
            //}
        }

        private static string toDateTime(DateTime date)
        {
            return $"{date.Year}-{date.Month}-{date.Day} {date.Hour}:{date.Minute}:{date.Second}:{date.Millisecond}";
        }

        private static void displayDocument(Data.Main.Entities.Tables.Document document)
        {
            Console.WriteLine("Id: " + document.Id);
            Console.WriteLine("CreationDate: " + document.CreationDate.ToString());
            Console.WriteLine("AddingTime: " + document.AddingTime.ToString());
            Console.WriteLine("Title: " + document.Title);
            Console.WriteLine("Issuer: " + document.Issuer);
            Console.WriteLine("Author: " + document.Author);
            Console.WriteLine("Description: " + document.Description);
            Console.WriteLine("Notes: " + document.Notes);
        }

        private static void displayDocuments(IEnumerable<Data.Main.Entities.Tables.Document> documents)
        {
            foreach (var document in documents)
            {
                displayDocument(document);
            }
        }
    }
}
