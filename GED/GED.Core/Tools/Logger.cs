﻿using NLog.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GED.Core.Tools
{
    public class Logger
    {
        public static void Log(Levels level,
           string message,
           [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
           [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
           [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            if (_nlogger == null)
            {
                initialise();
            }

            _nlogger.Log(getNLogLevel(level),
                message
                + " >--> " + memberName
                + " >--> " + sourceFilePath
                + " >--> " + sourceLineNumber);
        }
        public static void Log(Exception exception,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Log(Logger.Levels.Error,
                "Exception: " + exception?.Message,
                memberName,
                sourceFilePath,
                sourceLineNumber);
        }
        public static void Log(Core.Exceptions.NotFoundException exception,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Log(Logger.Levels.Warn,
                "NotFoundException: " + exception?.Message,
                memberName,
                sourceFilePath,
                sourceLineNumber);
        }
        public static void Log(Core.Exceptions.UnauthorizedException exception,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
            [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
            [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Logger.Log(Logger.Levels.Warn,
                "UnauthorizedException: " + exception?.Message,
                memberName,
                sourceFilePath,
                sourceLineNumber);
        }

        private static NLog.Logger _nlogger = null;
        private static void initialise()
        {
            _nlogger = NLog.LogManager.GetCurrentClassLogger();

            InternalLogger.LogFile = "nlog-start.log";
            InternalLogger.LogLevel = NLog.LogLevel.Trace;
        }

        public enum Levels : int
        {
            Trace = 0,
            Debug = 1,
            Info = 2,
            Warn = 3,
            Error = 4,
            Fatal = 5
        }
        private static NLog.LogLevel getNLogLevel(Levels level)
        {
            switch (level)
            {
                default:
                case Levels.Trace:
                    return NLog.LogLevel.Trace;

                case Levels.Debug:
                    return NLog.LogLevel.Debug;

                case Levels.Info:
                    return NLog.LogLevel.Info;

                case Levels.Warn:
                    return NLog.LogLevel.Warn;

                case Levels.Error:
                    return NLog.LogLevel.Error;

                case Levels.Fatal:
                    return NLog.LogLevel.Fatal;
            }
        }
    }
}
