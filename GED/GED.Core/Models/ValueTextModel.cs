﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class ValueTextModel
    {   public string Value { get; set; }
        public string Text { get; set; }

        public ValueTextModel()
        { }
        public ValueTextModel(string value, string text)
        {
            Value = value;
            Text = text;
        }
    }
}
