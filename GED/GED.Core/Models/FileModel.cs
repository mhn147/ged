﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public string Path { get; set; }
    }
}
