﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class ResponseModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Success { get; set; }
        public List<string> Infos { get; set; }
        public List<string> Warnings { get; set; }
        public List<string> Errors { get; set; }

        public string InfosCombined
        {
            get
            {
                return (Infos != null && Infos.Count > 0)
                    ? string.Join(", ", this.Infos)
                    : "";
            }
        }
        public string InfosLinesCombined
        {
            get
            {
                return (Infos != null && Infos.Count > 0)
                    ? string.Join("\n", this.Infos)
                    : "";
            }
        }

        public string WarningsCombined
        {
            get
            {
                return (Warnings != null && Warnings.Count > 0)
                    ? string.Join(", ", this.Warnings)
                    : "";
            }
        }
        public string WarningsLinesCombined
        {
            get
            {
                return (Warnings != null && Warnings.Count > 0)
                    ? string.Join("\n", this.Warnings)
                    : "";
            }
        }

        public string ErrorsCombined
        {
            get
            {
                return (Errors != null && Errors.Count > 0)
                    ? string.Join(", ", this.Errors)
                    : "";
            }
        }

        public string ErrorsLinesCombined
        {
            get
            {
                return (Errors != null && Errors.Count > 0)
                    ? string.Join("\n", this.Errors)
                    : "";
            }
        }

        public ResponseModel()
        {
            Success = false;
            Infos = new List<string>();
            Warnings = new List<string>();
            Errors = new List<string>();
        }
    }
}
