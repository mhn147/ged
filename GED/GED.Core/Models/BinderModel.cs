﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class BinderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
