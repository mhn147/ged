﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class FullDocumentModel
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime AddingTime { get; set; }

        public List<BinderModel> Binders { get; set; }
        public List<TagModel> Tags { get; set; }
        public List<LanguageModel> Languages { get; set; }

        public string Title { get; set; }
        public string Issuer { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
    }
}
