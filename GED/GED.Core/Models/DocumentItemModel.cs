﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class DocumentItemModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Issuer { get; set; }
        public List<string> Binders { get; set; }
    }
}
