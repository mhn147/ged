﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Models
{
    public class DocumentModel
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime AddingTime { get; set; }
        public List<string> Languages { get; set; }
        public List<string> Binders { get; set; }
        public List<string> Tags { get; set; }
        public string Title { get; set; }
        public string Issuer { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
    }
}
