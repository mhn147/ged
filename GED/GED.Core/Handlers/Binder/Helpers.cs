﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.Binder
{
    internal class Helpers
    {
        public static BinderModel ToBinder(Data.Main.Entities.Tables.Binder dbEntity)
        {
            var binderModel = new BinderModel();
            binderModel.Id = dbEntity.Id;
            binderModel.Name = dbEntity.Name;

            return binderModel;
        }
    }
}