﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.Binder
{
    public partial class Binders
    {
        public static List<Models.BinderModel> Get()
        {
            try
            {
                var result = new List<Models.BinderModel>();

                var bindersDb = Data.Main.Access.Tables.Binder.Get();

                foreach (var binderDb in bindersDb)
                {
                    result.Add(Helpers.ToBinder(binderDb));
                }

                return result;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
