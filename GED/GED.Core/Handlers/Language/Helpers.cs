﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.Language
{
    internal class Helpers
    {
        public static LanguageModel ToLanguage(Data.Main.Entities.Tables.Language dbEntity)
        {
            var languageModel = new LanguageModel();
            languageModel.Id = dbEntity.Id;
            languageModel.Name = dbEntity.Name;

            return languageModel;
        }
    }
}
