﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.Language
{
    public partial class Languages
    {
        public static List<Models.LanguageModel> Get()
        {
            try
            {
                var result = new List<Models.LanguageModel>();

                var languagesDb = Data.Main.Access.Tables.Language.Get();

                foreach (var langDb in languagesDb)
                {
                    result.Add(Helpers.ToLanguage(langDb));
                }

                return result;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
