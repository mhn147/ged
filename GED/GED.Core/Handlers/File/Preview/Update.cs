﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Preview
{
    public partial class PreviewFile
    {
        public static ResponseModel Update(FileModel model)
        {
            try
            {
                var response = new ResponseModel();

                var customerDb = Data.Main.Access.Tables.Document.GetPreviewPath(model.Id);
                if (customerDb == null)
                {
                    response.Errors.Add("Client introuvable");
                    return response;
                }

                Data.Main.Access.Tables.Document.UpdatePreviewFile(Helpers.ToDataEntity(model));

                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}

