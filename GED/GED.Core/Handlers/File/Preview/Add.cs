﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Preview
{
    public partial class PreviewFile
    {
        public static ResponseModel Add(FileModel model)
        {
            try
            {
                var response = new Models.ResponseModel();

                var dataEntity = Helpers.ToDataEntity(model);
                Data.Main.Access.Tables.Document.InsertPreviewPath(dataEntity.Id, dataEntity.PreviewPath);

                response.Id = dataEntity.Id.ToString();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
