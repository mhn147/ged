﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Preview
{
    internal class Helpers
    {
        public static GED.Data.Main.Entities.Tables.Document ToDataEntity(FileModel model)
        {
            return new Data.Main.Entities.Tables.Document()
            {
                Id = model.Id,
                PreviewPath = model.Path
            };
        }

        public static FileModel ToDocumentFile(Data.Main.Entities.Tables.Document dbEntity)
        {
            return new FileModel()
            {
                Id = dbEntity.Id,
                Path = dbEntity.PreviewPath
            };
        }
    }
}
