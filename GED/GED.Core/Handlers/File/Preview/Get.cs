﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Preview
{
    public partial class PreviewFile
    {
        public static Models.FileModel Get(int id)
        {
            try
            {
                var documentDb = Data.Main.Access.Tables.Document.GetPreviewPath(id);
                if (documentDb == null)
                {
                    throw new Exceptions.NotFoundException();
                }

                return Helpers.ToDocumentFile(documentDb);
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
