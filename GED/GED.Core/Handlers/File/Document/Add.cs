﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Document
{
    public partial class DocumentFile
    {
        public static ResponseModel Add(FileModel model)
        {
            try
            {
                var response = new Models.ResponseModel();

                var dataEntity = Helpers.ToDataEntity(model);
                Data.Main.Access.Tables.Document.InsertDocumentPath(dataEntity.Id, dataEntity.DocumentPath);

                response.Id = dataEntity.Id.ToString();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
