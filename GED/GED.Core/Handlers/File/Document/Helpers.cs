﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Document
{
    internal class Helpers
    {
        public static GED.Data.Main.Entities.Tables.Document ToDataEntity(FileModel model)
        {
            return new Data.Main.Entities.Tables.Document()
            {
                Id = model.Id,
                DocumentPath = model.Path
            };
        }

        public static FileModel ToDocumentFile(Data.Main.Entities.Tables.Document dbEntity)
        {
            return new FileModel()
            {
                Id = dbEntity.Id,
                Path = dbEntity.DocumentPath
            };
        }
    }
}
