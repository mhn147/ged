﻿using GED.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.File.Document
{
    public partial class DocumentFile
    {
        public static ResponseModel Delete(FileModel model)
        {
            try
            {
                var response = new ResponseModel();

                var customerDb = Data.Main.Access.Tables.Document.GetPreviewPath(model.Id);
                if (customerDb == null)
                {
                    response.Errors.Add("Client introuvable");
                    return response;
                }

                Data.Main.Access.Tables.Document.DeleteDocumentFile(Helpers.ToDataEntity(model).Id);

                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
