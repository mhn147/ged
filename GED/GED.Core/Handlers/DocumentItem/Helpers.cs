﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.DocumentItem
{
    internal class Helpers
    {
        public static GED.Data.Main.Entities.Tables.Document ToDataEntity(DocumentItemModel model)
        {
            return new Data.Main.Entities.Tables.Document()
            {
                Id = model.Id,
                Title = model.Title,
                Issuer = model.Issuer,
            };
        }

        public static DocumentItemModel ToDocumentItem(Data.Main.Entities.Tables.Document dbEntity)
        {
            return new DocumentItemModel()
            {
                Id = dbEntity.Id,
                Title = dbEntity.Title,
                Issuer = dbEntity.Issuer,
                Binders = Data.Main.Access.Tables.Document.GetDocumentBinders(dbEntity.Id)
            };
        }
    }
}
