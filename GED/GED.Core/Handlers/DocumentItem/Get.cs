﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.DocumentItem
{
    public partial class DocumentItems
    {
        public static List<Models.DocumentItemModel> Get()
        {
            try
            {
                var result = new List<Models.DocumentItemModel>();

                var documentsDb = Data.Main.Access.Tables.Document.Get();
                foreach (var documentDb in documentsDb)
                {
                    result.Add(Helpers.ToDocumentItem(documentDb));
                }

                return result;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }

        public static Models.DocumentItemModel Get(int id)
        {
            try
            {
                var documentDb = Data.Main.Access.Tables.Document.GetPreviewPath(id);
                if (documentDb == null)
                {
                    throw new Exceptions.NotFoundException();
                }

                return Helpers.ToDocumentItem(documentDb);
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
