﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.Tag
{
    public partial class Tags
    {
        public static List<Models.TagModel> Get()
        {
            try
            {
                var result = new List<Models.TagModel>();

                var tagsDb = Data.Main.Access.Tables.Tag.Get();

                foreach (var tagDb in tagsDb)
                {
                    result.Add(Helpers.ToTag(tagDb));
                }

                return result;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}