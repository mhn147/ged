﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.Tag
{
    internal class Helpers
    {
        public static TagModel ToTag(Data.Main.Entities.Tables.Tag dbEntity)
        {
            var tagModel = new TagModel();
            tagModel.Id = dbEntity.Id;
            tagModel.Name = dbEntity.Name;

            return tagModel;
        }
    }
}