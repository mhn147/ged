﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Core.Handlers.Document
{
    public partial class Documents
    {
        public static List<Models.DocumentModel> Get()
        {
            try
            {
                var result = new List<Models.DocumentModel>();

                var documentsDb = Data.Main.Access.Tables.Document.Get();
                foreach (var documentDb in documentsDb)
                {
                    result.Add(Helpers.ToDocument(documentDb));
                }

                return result;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }

        public static Models.DocumentModel Get(int id)
        {
            try
            {
                var documentDb = Data.Main.Access.Tables.Document.GetPreviewPath(id);
                if (documentDb == null)
                {
                    throw new Exceptions.NotFoundException();
                }

                return Helpers.ToDocument(documentDb);
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
