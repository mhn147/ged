﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.Document
{
    internal class Helpers
    {
        public static DocumentModel ToDocument(Data.Main.Entities.Tables.Document dbEntity)
        {
            var documentModel = new DocumentModel();
            documentModel.Id = dbEntity.Id;
            documentModel.CreationDate = dbEntity.CreationDate;
            documentModel.Author = dbEntity.Author;
            documentModel.AddingTime = dbEntity.AddingTime;
            documentModel.Title = dbEntity.Title;
            documentModel.Issuer = dbEntity.Issuer;
            documentModel.Description = dbEntity.Description;
            documentModel.Notes = dbEntity.Notes;
            documentModel.Languages = Data.Main.Access.Tables.Document.GetDocumentLanguages(dbEntity.Id);
            documentModel.Binders = Data.Main.Access.Tables.Document.GetDocumentBinders(dbEntity.Id);
            documentModel.Tags = Data.Main.Access.Tables.Document.GetDocumentTags(dbEntity.Id);


            return documentModel;
        }

        public static Data.Main.Entities.Tables.Document ToDataEntity(DocumentModel document)
        {
            return new Data.Main.Entities.Tables.Document()
            {
                Id = document.Id,
                CreationDate = document.CreationDate,
                AddingTime = document.AddingTime,
                Title = document.Title,
                Issuer = document.Issuer,
                Author = document.Author,
                Description = document.Description,
                Notes = document.Notes
            };
        }
    }
}
