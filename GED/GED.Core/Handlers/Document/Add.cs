﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Core.Models;

namespace GED.Core.Handlers.Document
{
    public partial class Documents
    {
        public static ResponseModel Add(DocumentModel model, List<int> bindersIds, List<int> tagsIds, List<int> languagesIds)
        {
            try
            {
                var response = new Models.ResponseModel();

                var dataEntity = Helpers.ToDataEntity(model);

                var insertedId = Data.Main.Access.Tables.Document.Insert(dataEntity);

                Data.Main.Access.Tables.Document.InsertDocumentBinders(insertedId, bindersIds);
                Data.Main.Access.Tables.Document.InsertDocumentTags(insertedId, tagsIds);
                Data.Main.Access.Tables.Document.InsertDocumentLanguages(insertedId, languagesIds);

                response.Id = insertedId.ToString();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                Tools.Logger.Log(e);
                throw e;
            }
        }
    }
}
