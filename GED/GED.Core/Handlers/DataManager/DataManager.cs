﻿using System;
using System.Collections.Generic;
using System.Text;
using GED.Data;

namespace GED.Core.Handlers
{
    public class DataManager
    {
        public static void SetConnectionStrings(string mainConnectionString)
        {
            Data.Main.Access.Settings.SetConnectionStrings(mainConnectionString);
        }
    }
}
