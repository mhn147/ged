﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace GED.Data.Main.Entities.Tables
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Language()
        {

        }

        public Language(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["Id"]);
            Name = Convert.ToString(dataRow["Name"]);
        }

        public Language(Language language)
        {
            this.Id = language.Id;
            this.Name = language.Name;
        }
    }
}
