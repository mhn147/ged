﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace GED.Data.Main.Entities.Tables
{
    public class DocumentLanguages
    {
        public List<string> Languages { get; set; }

        public DocumentLanguages()
        {

        }

        public DocumentLanguages(string language)
        {
            Languages.Add(language);
        }

        public DocumentLanguages(DocumentLanguages documentLanguages)
        {
            this.Languages = documentLanguages.Languages;
        }
    }
}
