﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace GED.Data.Main.Entities.Tables
{
    public class DocumentBinders
    {
        public List<string> Binders { get; set; }

        public DocumentBinders()
        {

        }

        public DocumentBinders(string binder)
        {
           
            Binders.Add(binder);
        }

        public DocumentBinders(DocumentBinders documentBinders)
        {
            this.Binders = documentBinders.Binders;
        }
    }
}
