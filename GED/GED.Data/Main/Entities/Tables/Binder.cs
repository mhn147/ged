﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace GED.Data.Main.Entities.Tables
{
    public class Binder
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Binder()
        {

        }

        public Binder(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["Id"]);
            Name = Convert.ToString(dataRow["Name"]);
        }

        public Binder(Binder binder)
        {
            this.Id = binder.Id;
            this.Name = binder.Name;
        }
    }
}
