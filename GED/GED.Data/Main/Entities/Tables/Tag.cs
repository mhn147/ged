﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace GED.Data.Main.Entities.Tables
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Tag()
        {

        }

        public Tag(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["Id"]);
            Name = Convert.ToString(dataRow["Tag"]);
        }

        public Tag(Tag tag)
        {
            this.Id = tag.Id;
            this.Name = tag.Name;
        }
    }

}
