﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace GED.Data.Main.Entities.Tables
{
    public class Document
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime AddingTime { get; set; }
        public string Title { get; set; }
        public string Issuer { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string DocumentPath { get; set; }
        public string PreviewPath { get; set; }
        public string FileName { get; set; }


        public Document()
        {

        }

        public Document(DataRow dataRow)
        {
            Id = Convert.ToInt32(dataRow["Id"]);
            CreationDate = Convert.ToDateTime(dataRow["CreationDate"]);
            AddingTime = Convert.ToDateTime(dataRow["AddingTime"]);
            Title = Convert.ToString(dataRow["Title"]);
            Issuer = Convert.ToString(dataRow["Issuer"]);
            Author = Convert.ToString(dataRow["Author"]);
            Description = Convert.ToString(dataRow["Description"]);
            Notes = Convert.ToString(dataRow["Notes"]);
        }

        public Document(Document document)
        {
            Id = document.Id;
            CreationDate = document.CreationDate;
            AddingTime = document.AddingTime;
            Title = document.Title;
            Issuer = document.Issuer;
            Author = document.Author;
            Description = document.Description;
            Notes = document.Notes;
        }
    }
}
