﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace GED.Data.Main.Entities.Tables
{
    public class DocumentTags
    {
        public List<string> Tags { get; set; }

        public DocumentTags()
        {

        }

        public DocumentTags(string tag)
        {
            Tags.Add(tag);
        }

        public DocumentTags(DocumentTags documentTags)
        {
            this.Tags = documentTags.Tags;
        }
    }
}
