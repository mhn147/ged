﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GED.Data.Main.Access
{
    public class Settings
    {
        private static string _mainConnectionString { get; set; }

        public const int MaxQueryParamsNumber = 100;

        public static void SetConnectionStrings(string mainConnectionString)
        {
            _mainConnectionString = mainConnectionString;
        }

        public static string MainConnectionString
        {
            get
            {
                return _mainConnectionString;
            }
        }
    }
}
