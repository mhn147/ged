﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GED.Data.Main.Entities.Tables;

namespace GED.Data.Main.Access.Tables
{
    public class Tag
    {
        #region default methods

        public static IEnumerable<Entities.Tables.Tag> Get()
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "Select * from dbo.Tag;";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            foreach (DataRow dataRow in dataTable.Rows)
            {
                yield return new Entities.Tables.Tag(dataRow);
            }
        }

        #endregion
    }
}
