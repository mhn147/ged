﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using GED.Data.Main.Entities.Tables;

namespace GED.Data.Main.Access.Tables
{
    public class Document
    {
#region default methods
        public static Entities.Tables.Document GetPreviewPath(int id)
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "SELECT document.Id, document.CreationDate, document.AddingTime, document.Title, document.Author, document.Issuer, document.Description, document.Notes FROM dbo.Document as document WHERE document.Id = @Id";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            if (dataTable.Rows.Count > 0)
            {
                return new Entities.Tables.Document(dataTable.Rows[0]);
            }
            else
            {
                return null;
            }
        }
        public static IEnumerable<Entities.Tables.Document> Get()
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "SELECT document.Id, document.CreationDate, document.AddingTime, document.Title, document.Author, document.Issuer, document.Description, document.Notes FROM dbo.Document as document;";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            foreach (DataRow dataRow in dataTable.Rows)
            {
                yield return new Entities.Tables.Document(dataRow);
            }
        }
        private static IEnumerable<Entities.Tables.Document> get(List<int> ids)
        {
            if (ids == null)
            {
                yield break;
            }

            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                var sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;

                var idsString = string.Empty;
                for (int i = 0; i < ids.Count; i++)
                {
                    idsString += "@Id" + i + ",";
                    sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                }
                idsString = idsString.TrimEnd(',');

                sqlCommand.CommandText = "SELECT document.Id, document.CreationDate, document.AddingTime, document.Title, document.Author, document.Issuer, document.Description, document.Notes FROM dbo.Document as document WHERE document.Id IN (" + idsString + ")";

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            foreach (DataRow dataRow in dataTable.Rows)
            {
                yield return new Entities.Tables.Document(dataRow);
            }
        }
        public static IEnumerable<Entities.Tables.Document> Get(List<int> ids)
        {
            if (ids == null)
            {
                yield break;
            }

            int maxQueryParamsNumber = Settings.MaxQueryParamsNumber;

            if (ids.Count <= maxQueryParamsNumber)
            {
                foreach (var item in get(ids))
                {
                    yield return item;
                }
            }
            else
            {
                int batchNumber = ids.Count / maxQueryParamsNumber;
                for (int i = 0; i < batchNumber; i++)
                {
                    foreach (var item in get(ids.GetRange(i * maxQueryParamsNumber, maxQueryParamsNumber)))
                    {
                        yield return item;
                    }
                }
                foreach (var item in get(ids.GetRange(batchNumber * maxQueryParamsNumber, ids.Count - batchNumber * maxQueryParamsNumber)))
                {
                    yield return item;
                }
            }
        }
        public static int Insert(Entities.Tables.Document element)
        {
            int response = -1;

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "INSERT INTO dbo.Document (CreationDate, AddingTime, Title, " +
                    "Issuer, Author, Description, Notes) " +
                    "VALUES (@CreationDate, @AddingTime, @Title, @Issuer, @Author, @Description, " +
                    "@Notes)";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                //sqlCommand.Parameters.AddWithValue("Id", element.Id == 0 ? (object)DBNull.Value : element.CreationDate);
                sqlCommand.Parameters.AddWithValue("CreationDate", element.CreationDate == null ? (object)DBNull.Value : element.CreationDate);
                sqlCommand.Parameters.AddWithValue("AddingTime", element.AddingTime == null ? (object)DBNull.Value : element.AddingTime);
                sqlCommand.Parameters.AddWithValue("Title", element.AddingTime == null ? (object)DBNull.Value : element.Title);
                sqlCommand.Parameters.AddWithValue("Issuer", element.AddingTime == null ? (object)DBNull.Value : element.Issuer);
                sqlCommand.Parameters.AddWithValue("Author", element.AddingTime == null ? (object)DBNull.Value : element.Author);
                sqlCommand.Parameters.AddWithValue("Description", element.AddingTime == null ? (object)DBNull.Value : element.Description);
                sqlCommand.Parameters.AddWithValue("Notes", element.AddingTime == null ? (object)DBNull.Value : element.Notes);
                response = sqlCommand.ExecuteNonQuery();

                if (response > 0)
                {
                    query = "SELECT Id FROM dbo.Document WHERE Id = @@IDENTITY";
                    sqlCommand = new SqlCommand(query, sqlConnection);
                    object insertedId = sqlCommand.ExecuteScalar();
                    if (insertedId != null)
                    {
                        response = Convert.ToInt32(insertedId.ToString());
                    }
                    else
                    {
                        response = -1;
                    }
                }
                else
                {
                    response = -1;
                }
            }

            return response;
        }
        public static void Update(Entities.Tables.Document element)
        {
            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "UPDATE dbo.Document SET " +
                               "CreationDate=@CreationDate, " +
                               "AddingTime=@AddingTime, Title=@Title, " +
                               "Issuer=@Issuer, Author=@Author, Description=@Description, " +
                               "Notes=@Notes " + 
                               "WHERE Id = @Id";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", element.Id);
                sqlCommand.Parameters.AddWithValue("CreationDate", element.CreationDate);
                sqlCommand.Parameters.AddWithValue("AddingTime", element.AddingTime);
                sqlCommand.Parameters.AddWithValue("Title", element.Title);
                sqlCommand.Parameters.AddWithValue("Issuer", element.Issuer);
                sqlCommand.Parameters.AddWithValue("Author", element.Author);
                sqlCommand.Parameters.AddWithValue("Description", element.Description);
                sqlCommand.Parameters.AddWithValue("Notes", element.Notes);

                sqlCommand.ExecuteNonQuery();
            }
        }
        public static int Delete(int id)
        {
            int response = -1;

            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "DELETE FROM dbo.Document WHERE Id = @Id";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);

                response = sqlCommand.ExecuteNonQuery();
            }

            return response;
        }
        private static int delete(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                int response = -1;

                using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
                {
                    sqlConnection.Open();

                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConnection;

                    string queryIds = string.Empty;
                    for (int i = 0; i < ids.Count; i++)
                    {
                        queryIds += "@Id" + i + ",";
                        sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                    }
                    queryIds = queryIds.TrimEnd(',');

                    string query = "DELETE FROM dbo.Document WHERE Id IN (" + queryIds + ")";
                    sqlCommand.CommandText = query;

                    response = sqlCommand.ExecuteNonQuery();
                }

                return response;
            }
            return -1;
        }
        public static int Delete(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                int maxParamsNumber = Data.Main.Access.Settings.MaxQueryParamsNumber;
                int result = 0;
                if (ids.Count <= maxParamsNumber)
                {
                    result = delete(ids);
                }
                else
                {
                    int batchNumber = ids.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        result += delete(ids.GetRange(i * maxParamsNumber, maxParamsNumber));
                    }
                    result += delete(ids.GetRange(batchNumber * maxParamsNumber, ids.Count - batchNumber * maxParamsNumber));
                }
            }
            return -1;

        }
        #endregion

        #region custom
        public static List<string> GetDocumentBinders(int id)
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "SELECT binder.Name as Binder FROM dbo.Document as document, dbo.DocumentBinder as documentBinder, dbo.Binder as binder WHERE document.Id = @Id and document.Id = documentBinder.DocumentId and documentBinder.BinderId = binder.Id; ";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            var docBinders = new List<string>();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                docBinders.Add(dataRow["Binder"].ToString());
            }

            return docBinders;
        }
        public static List<string> GetDocumentLanguages(int id)
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "SELECT language.Name as Language FROM dbo.Document as document, dbo.DocumentLanguage as documentLanguage, dbo.Language as language WHERE document.Id = @Id and document.Id = documentLanguage.DocumentId and documentLanguage.LanguageId = language.Id;";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            var docLangs = new List<string>();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                docLangs.Add(dataRow["Language"].ToString());
            }

            return docLangs;
        }
        public static List<string> GetDocumentTags(int id)
        {
            var dataTable = new DataTable();

            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "SELECT tag.Tag FROM dbo.Document as document, dbo.DocumentTag as documentTag, dbo.Tag as tag" +
                               " WHERE document.Id = @Id and document.Id = documentTag.DocumentId and documentTag.TagId = tag.Id;";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            var docTags = new List<string>();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                docTags.Add(dataRow["Tag"].ToString());
            }

            return docTags;
        }
        public static int InsertDocumentTag(int documentId, int tagId)
        {
            int response = -1;

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "INSERT INTO dbo.DocumentTag (DocumentId, TagId)" +
                    "VALUES (@DocumentId, @TagId)";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("DocumentId", documentId == 0 ? (object)DBNull.Value : documentId);
                sqlCommand.Parameters.AddWithValue("TagId", tagId == 0 ? (object)DBNull.Value : tagId);
                response = sqlCommand.ExecuteNonQuery();

                if (response > 0)
                {
                    query = "SELECT Id FROM dbo.DocumentTag WHERE Id = @@IDENTITY";
                    sqlCommand = new SqlCommand(query, sqlConnection);
                    object insertedId = sqlCommand.ExecuteScalar();
                    if (insertedId != null)
                    {
                        response = Convert.ToInt32(insertedId.ToString());
                    }
                    else
                    {
                        response = -1;
                    }
                }
                else
                {
                    response = -1;
                }
            }

            return response;
        }
        public static int InsertDocumentTags(int documentId, List<int> tagsIDs)
        {
            int response = -1;

            foreach (var tagId in tagsIDs)
            {
                response = InsertDocumentTag(documentId, tagId);

                if (response == -1)
                    return response;
            }

            return response;
        } 
        public static int InsertDocumentLanguage(int documentId, int languageId)
        {
            int response = -1;

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "INSERT INTO dbo.DocumentLanguage (DocumentId, LanguageId)" +
                    "VALUES (@DocumentId, @LanguageId)";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("DocumentId", documentId == 0 ? (object)DBNull.Value : documentId);
                sqlCommand.Parameters.AddWithValue("LanguageId", languageId == 0 ? (object)DBNull.Value : languageId);
                response = sqlCommand.ExecuteNonQuery();

                if (response > 0)
                {
                    query = "SELECT Id FROM dbo.DocumentLanguage WHERE Id = @@IDENTITY";
                    sqlCommand = new SqlCommand(query, sqlConnection);
                    object insertedId = sqlCommand.ExecuteScalar();
                    if (insertedId != null)
                    {
                        response = Convert.ToInt32(insertedId.ToString());
                    }
                    else
                    {
                        response = -1;
                    }
                }
                else
                {
                    response = -1;
                }
            }

            return response;
        }
        public static int InsertDocumentLanguages(int documentId, List<int> languagesIds)
        {
            int response = -1;

            foreach (var languageId in languagesIds)
            {
                response = InsertDocumentLanguage(documentId, languageId);

                if (response == -1)
                    return response;
            }

            return response;
        }
        public static int InsertDocumentBinder(int documentId, int binderId)
        {
            int response = -1;

            using (var sqlConnection = new SqlConnection(Data.Main.Access.Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "INSERT INTO dbo.DocumentBinder (DocumentId, BinderId)" +
                    "VALUES (@DocumentId, @BinderId)";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("DocumentId", documentId == 0 ? (object)DBNull.Value : documentId);
                sqlCommand.Parameters.AddWithValue("BinderId", binderId == 0 ? (object)DBNull.Value : binderId);
                response = sqlCommand.ExecuteNonQuery();

                if (response > 0)
                {
                    query = "SELECT Id FROM dbo.DocumentBinder WHERE Id = @@IDENTITY";
                    sqlCommand = new SqlCommand(query, sqlConnection);
                    object insertedId = sqlCommand.ExecuteScalar();
                    if (insertedId != null)
                    {
                        response = Convert.ToInt32(insertedId.ToString());
                    }
                    else
                    {
                        response = -1;
                    }
                }
                else
                {
                    response = -1;
                }
            }

            return response;
        }
        public static int InsertDocumentBinders(int documentId, List<int> bindersIds)
        {
            int response = -1;

            foreach (var binderId in bindersIds)
            {
                response = InsertDocumentBinder(documentId, binderId);

                if (response == -1)
                    return response;

            }

            return response;
        }
        public static void DeletePreviewFile(int id)
        {
            throw new NotImplementedException();
        }
        public static void UpdateDocumentFile(Entities.Tables.Document document)
        {
            throw new NotImplementedException();
        }
        public static void InsertPreviewPath(int id, string documentPath)
        {
            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "UPDATE dbo.Document SET " +
                               "PreviewPath=@PreviewPath " +
                               "WHERE Id = @Id";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);
                sqlCommand.Parameters.AddWithValue("PreviewPath", documentPath);

                sqlCommand.ExecuteNonQuery();
            }
        }
        public static void UpdatePreviewFile(Entities.Tables.Document document)
        {
            throw new NotImplementedException();
        }
        public static void DeleteDocumentFile(int id)
        {
            throw new NotImplementedException();
        }
        public static void InsertDocumentPath(int id, string documentPath)
        {
            using (var sqlConnection = new SqlConnection(Settings.MainConnectionString))
            {
                sqlConnection.Open();

                string query = "UPDATE dbo.Document SET " +
                               "FilePath=@DocumentPath " +
                               "WHERE Id = @Id";

                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", id);
                sqlCommand.Parameters.AddWithValue("DocumentPath", documentPath);

                sqlCommand.ExecuteNonQuery();
            }
        }
        #endregion
    }
}