﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace GED.Controllers
{
    [Route("api/tags")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class TagsController : Controller
    {
        [EnableCors("MyPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(Core.Handlers.Tag.Tags.Get());
        }
    }
}
