﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GED.Core;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GED.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/documentitems")]
    public class DocumentItemsController : Controller
    {
        // GET: api/<controller>
        [EnableCors("MyPolicy")]
        [HttpGet]
        public IEnumerable<Core.Models.DocumentItemModel> Get()
        {
            return GED.Core.Handlers.DocumentItem.DocumentItems.Get();
        }
    }
}
