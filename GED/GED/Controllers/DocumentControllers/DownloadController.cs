﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Cors;

namespace GED.Controllers
{
    [EnableCors]
    public class DownloadController : Controller
    {
        [EnableCors("MyPolicy")]
        [HttpGet]
        [Route("api/download/{fileId}/{ext}")]
        public IActionResult Get(string fileId, string ext)
        {
            string filePath = Startup.Configuration["Files:Path"] + fileId + "." + ext;

            var stream = new FileStream(filePath, FileMode.Open);

            if (ext == "pdf")
            {
                return new FileStreamResult(stream, "application/pdf");
            }

            return new FileStreamResult(stream, "image/" + ext);
        }
    }
}
