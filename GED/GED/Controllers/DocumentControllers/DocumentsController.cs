﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Net.Http.Headers;
using System.Text;

namespace GED.Controllers
{
    [Produces("application/json")]
    [Route("api/documents")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class DocumentsController : Controller
    {
        [EnableCors("MyPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(Core.Handlers.Document.Documents.Get());
        }

        // GET api/documents/5
        [EnableCors("MyPolicy")]
        [HttpGet("{id}")]
        public Core.Models.DocumentModel Get(int id)
        {
            return Core.Handlers.Document.Documents.Get(id);
        }

        [EnableCors("MyPolicy")]
        [HttpPost]
        public ActionResult Post([FromForm]Core.Models.FullDocumentModel fullDocumentModel)
        {
            return Json("lol");
        }
    }
}
