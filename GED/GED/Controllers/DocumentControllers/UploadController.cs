﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GED.Controllers
{
    [Produces("application/json")]
    [EnableCors("MyPolicy")]
    public class UploadController : Controller
    {
        [EnableCors("MyPolicy")]
        [HttpPost, DisableRequestSizeLimit]
        [Route("api/upload/document")]
        public ActionResult UploadDocument()
        {
            try
            {
                var file = Request.Form.Files[0];

                var extension = file.ContentType.Substring(file.ContentType.IndexOf("/") + 1);

                string dirPath = Startup.Configuration["Files:Path"];

                string fullPath = "";

                string fileName = "";

                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                if (file.Length > 0)
                {
                    fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(dirPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                Core.Handlers.File.Document.DocumentFile.Add(new Core.Models.FileModel() { Id = int.Parse(file.FileName.Substring(0, file.FileName.IndexOf("."))), Path = fullPath});
                return Json("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }

        [EnableCors("MyPolicy")]
        [HttpPost, DisableRequestSizeLimit]
        [Route("api/upload/preview")]
        public ActionResult UploadPreview()
        {
            try
            {
                var file = Request.Form.Files[0];

                var extension = file.ContentType.Substring(file.ContentType.IndexOf("/") + 1);

                string dirPath = Startup.Configuration["Files:Path"];

                string fullPath = "";

                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fullPath = Path.Combine(dirPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                Core.Handlers.File.Preview.PreviewFile.Add(new Core.Models.FileModel() { Id = int.Parse(file.FileName.Substring(0, file.FileName.IndexOf("."))), Path = fullPath });
                return Json("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }
    }
}
