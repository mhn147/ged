﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;


namespace GED.Controllers
{
    [Route("api/languages")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class LanguagesController : Controller
    {
        [EnableCors("MyPolicy")]
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(Core.Handlers.Language.Languages.Get());
        }
    }
}