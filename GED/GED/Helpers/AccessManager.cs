﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GED.Helpers
{
    internal class AccessManager
    {
        public static void SetConnectionString(string connStr)
        {
            Core.Handlers.DataManager.SetConnectionStrings(connStr);
        }

        public static int add(int a, int b)
        {
            return a + b;
        }
    }
}
